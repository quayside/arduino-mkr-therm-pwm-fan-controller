# arduino-mkr-therm-pwm-fan-controller

Quickly hacked together code to control a standard PWM PC fan using an Arduino MKR board and MKR Therm Shield. 
Uses Pin 6 for PWM control.

References:

https://forum.arduino.cc/t/solved-mkr1000-pwm-timers/437287/2

https://create.arduino.cc/projecthub/KaptenJansson/pwn-fan-controller-with-temp-sensing-and-button-override-4f2e8d

https://github.com/mariuste/Fan_Temp_Control